﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace XmasTree {
    public partial class MainWindow : Window {
        // Constants used throughout the program.
        public static Thickness GridMargin = new Thickness(5.0);
        public static Thickness ElementMargin = new Thickness(5.0);
        public static byte RowCountMin = 2;
        public static byte RowCountMax = 100;

        public MainWindow() {
            InitializeComponent();
        }

        // Runs when user presses Enter while one of the input text boxes is selected.
        // Currently used only for simulating clicking on "Generate" button when the user presses Enter while one of the input text boxes is selected.
        private void onTextBoxPreviewKeyDown(object sender, KeyEventArgs eventArgs) {
            if (eventArgs.Key == Key.Enter) {
                onGenerateClick(sender, eventArgs);
                eventArgs.Handled = true;
            }
        }

        // Runs when the user clicks "Generate" button.
        // Generates the greeting card: its header message, the Christmas Tree (body) and footer message.
        private void onGenerateClick(object sender, RoutedEventArgs eventArgs) {
            try {
                // Test for the valid input.
                // If the input is not valid an error message box will be displayed.
                var rowCount = byte.Parse(textBoxRowCount.Text);
                if (rowCount < RowCountMin || rowCount > RowCountMax) {
                    throw new OverflowException("Value outside of allowed range");
                }

                // Add the header to the message.
                textBoxXmasTree.Text = textBoxHeader.Text + "\n\n";

                /* CHRISTMAS TREE GENERATION */

                var midPoint = --rowCount - 1;
                textBoxXmasTree.Text += string.Concat(Enumerable.Repeat(" ", midPoint)) + "*\n";

                rowCount--;
                for (byte i = 1;i < rowCount;++i) {
                    textBoxXmasTree.Text += string.Concat(Enumerable.Repeat(" ", midPoint - i)) + "*" + string.Concat(Enumerable.Repeat(" ", 2 * i - 1)) + "*\n";
                }

                if (rowCount != 0) {
                    textBoxXmasTree.Text += string.Concat(Enumerable.Repeat("*", 2 * midPoint + 1)) + "\n";
                }

                // Finish the generation of the Christmas Tree and add the footer message.
                textBoxXmasTree.Text += textBoxXmasTree.GetLineText(2) + "\n" + textBoxFooter.Text;

                // Now the "Copy" button can be enabled.
                buttonCopy.IsEnabled = true;
            } catch (FormatException ex) {
                _ = ex;
                MessageBox.Show("Row count field must contain an integer.", "Bad row count format", MessageBoxButton.OK, MessageBoxImage.Error);
            } catch (OverflowException ex) {
                _ = ex;
                MessageBox.Show(string.Format("Specified row count is not in the valid range of [{0}, {1}].", RowCountMin, RowCountMax), "Row count out of range", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        // Runs when the user clicks "Copy" button.
        // Copies the greeting card to the clipboard.
        private void onCopyClick(object sender, RoutedEventArgs eventArgs) {
            Clipboard.SetText(textBoxXmasTree.Text);
        }
    }
}
