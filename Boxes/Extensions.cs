using System.Collections.Generic;
using System.Linq;

namespace Boxes {
    public static class Extensions {
        // Returns a user-friendly string representation of the data.
        public static string ToStringUserFriendly(this Boxes.Box box) => string.Format("({0}, {1}, {2})", box.width, box.height, box.depth);
        public static string ToStringUserFriendly(this Boxes.Box[] boxArray) => string.Format("[{0}]", string.Join(", ", boxArray.Select(box => box.ToStringUserFriendly())));
        public static string ToStringUserFriendly(this List<Boxes.Box> boxList) => string.Format("[{0}]", string.Join(", ", boxList.Select(box => box.ToStringUserFriendly())));
        public static string ToStringUserFriendly(this List<List<Boxes.Box>> boxListList) => string.Format("{{{0}}}", string.Join(", ", boxListList.Select(boxList => boxList.ToStringUserFriendly())));
    }
}