﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Boxes {
    public sealed class Boxes {
        // Constants used throughout the program.
        private const int BoxDimensionLowerBound = 1;   // The lowest value a box dimension can be.
        private const int BoxDimensionUpperBound = 11;  // The largest value a box dimension can be.
        private const int BoxInputArrayLength = 10;     // Size of the input box array.

        // Box modeled as its dimensions: width, height and depth.
        public readonly record struct Box(byte width, byte height, byte depth);

        // Input box array and output lists produced by applying algorithms to it.
        private readonly Box[] boxArrayInput;               // The input box array.
        private readonly (List<Box>, uint) boxListHighest;  // The highest possible stack of boxes, bottom–top.
        private readonly List<List<Box>> boxListMinimal;    // The smallest possible grouping of box stacks, bottom–top.

        // Initializes the input box array from the argument and produces the output lists by applying algorithms to the input.
        public Boxes(Box[] boxArrayInput) {
            this.boxArrayInput = boxArrayInput;

            boxListHighest = computeBoxListHighest();
            boxListMinimal = computeBoxListMinimal().Select(boxList => boxList.Select(box => box.Item1).ToList()).ToList();
        }

        // Prints the input box array and its algorithm-produced outputs.
        public void printInfo() {
            Console.WriteLine("Input box array: {0}", boxArrayInput.ToStringUserFriendly());
            Console.WriteLine("Highest output box array (height = {0}): {1}", boxListHighest.Item2, boxListHighest.Item1.ToStringUserFriendly());
            Console.WriteLine("Minimal output box array grouping (groups = {0}): {1}", boxListMinimal.Count, boxListMinimal.ToStringUserFriendly());
        }

        // Applies an algorithm for finding the highest possible stack of boxes to the input box array and returns the result.
        private (List<Box>, uint) computeBoxListHighest((List<Box>, uint)? initBoxList = null, int skipN = 0) {
            var boxList = initBoxList ?? (new List<Box>(BoxInputArrayLength), 0U);
            var boxListArrayAlt = new List<(List<Box>, uint)>(BoxInputArrayLength - 1);

            for (int i = skipN;i < boxArrayInput.Length;++i) {
                for (int j = boxList.Item1.Count;j >= 0;--j) {
                    if (j == 0 || (boxArrayInput[i].width <= boxList.Item1[j - 1].width && boxArrayInput[i].depth <= boxList.Item1[j - 1].depth)) {
                        if (j == boxList.Item1.Count || (boxArrayInput[i].width >= boxList.Item1[j].width && boxArrayInput[i].depth >= boxList.Item1[j].depth)) {
                            boxList.Item1.Insert(j, boxArrayInput[i]);
                            boxList.Item2 += boxArrayInput[i].height;
                        } else {
                            var initBoxListNext = boxList.Item1.Take(j).ToList();
                            
                            var boxListSmallerBoxIndex = boxList.Item1.FindIndex(j + 1, box => boxArrayInput[i].width >= box.width && boxArrayInput[i].depth >= box.depth);
                            if (boxListSmallerBoxIndex != -1) {
                                initBoxListNext.AddRange(boxList.Item1.GetRange(boxListSmallerBoxIndex, boxList.Item1.Count - boxListSmallerBoxIndex));
                            }

                            boxListArrayAlt.Add(computeBoxListHighest((initBoxListNext, initBoxListNext.Aggregate(0U, (height, box) => height += box.height)), i));
                        }

                        break;
                    }
                }
            }

            var boxListHighest = boxList;

            foreach (var boxListAlt in boxListArrayAlt) {
                boxListHighest = boxListHighest.Item2 < boxListAlt.Item2 ? boxListAlt : boxListHighest;
            }

            return boxListHighest;
        }

        // Applies an algorithm for finding the smallest possible grouping of box stacks to the input box array and returns the result.
        private List<List<(Box, int)>> computeBoxListMinimal(int skipN = 0) {
            var boxList = new List<(Box, int)>(BoxInputArrayLength);
            var boxListArrayAlt = new List<List<(Box, int)>>(BoxInputArrayLength - 1);

            for (int i = skipN;i < boxArrayInput.Length;++i) {
                for (int j = boxList.Count;j >= 0;--j) {
                    if (j == 0 || (boxArrayInput[i].width <= boxList[j - 1].Item1.width && boxArrayInput[i].depth <= boxList[j - 1].Item1.depth)) {
                        if (j == boxList.Count || (boxArrayInput[i].width >= boxList[j].Item1.width && boxArrayInput[i].depth >= boxList[j].Item1.depth)) {
                            boxList.Insert(j, (boxArrayInput[i], i));
                        } else {
                            boxListArrayAlt.AddRange(computeBoxListMinimal(i));
                            i = boxArrayInput.Length - 1;
                        }

                        break;
                    }
                }
            }

            boxListArrayAlt.Add(boxList);

            for (int i = 0;i < boxListArrayAlt.Count - 1;++i) {
                var boxListAltPrimary = boxListArrayAlt[i];

                for (int j = i + 1;j < boxListArrayAlt.Count;++j) {
                    var boxListAltSecondary = boxListArrayAlt[j];

                    for (int k = 0;k < boxListAltSecondary.Count;++k) {
                        var boxAltSecondary = boxListAltSecondary[k];

                        for (int l = boxListAltPrimary.Count;l >= 0;--l) {
                            if ((l == 0 || (boxAltSecondary.Item1.width <= boxListAltPrimary[l - 1].Item1.width && boxAltSecondary.Item1.depth <= boxListAltPrimary[l - 1].Item1.depth))
                                && (l == boxListAltPrimary.Count || (boxAltSecondary.Item1.width >= boxListAltPrimary[l].Item1.width && boxAltSecondary.Item1.depth >= boxListAltPrimary[l].Item1.depth))) {
                                if (l == 0 || boxAltSecondary.Item2 != boxListAltPrimary[l - 1].Item2) {
                                    boxListAltPrimary.Insert(l, boxAltSecondary);
                                }

                                boxListAltSecondary.RemoveAt(k--);
                                break;
                            }
                        }
                    }

                    if (0 == boxListAltSecondary.Count) {
                        boxListArrayAlt.RemoveAt(j--);
                        i--;
                    }
                }
            }

            return boxListArrayAlt;
        }

        // Generates the input box array and returns it.
        // It uses the defaults from the constants section.
        public static Box[] generateInputBoxArray() {
            var rand = new Random();
            var boxArray = new Box[BoxInputArrayLength];

            for (int i = 0;i < boxArray.Length;++i) {
                boxArray[i] = new Box(
                    (byte) rand.Next(BoxDimensionLowerBound, BoxDimensionUpperBound),
                    (byte) rand.Next(BoxDimensionLowerBound, BoxDimensionUpperBound),
                    (byte) rand.Next(BoxDimensionLowerBound, BoxDimensionUpperBound)
                );
            }

            return boxArray;
        }

        // Does the deed.
        public static void Main() => (new Boxes(Boxes.generateInputBoxArray())).printInfo();
    }
}