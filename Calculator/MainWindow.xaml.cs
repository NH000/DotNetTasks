﻿using System.Globalization;
using System.Threading;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace Calculator {
    public partial class MainWindow : Window {
		// Constants used throughout the program.
		public static double DisplayFontSize = 100.0;
		public static double ButtonFontSize = 40.0;
		
		// Possible reasons why was the display updated.
		private enum DisplayUpdateReason {
			MANUAL,		// The display was updated by the user manually entering the digits.
			AUTOMATIC,	// The display update was automatically triggered by one of the sign or special buttons.
		}
		
		// The property corresponding to DisplayUpdateReason enum.
		private static readonly DependencyProperty propertyDisplayUpdateReason = DependencyProperty.RegisterAttached(
			"DisplayUpdateReason",
			typeof(DisplayUpdateReason),
			typeof(MainWindow),
			new PropertyMetadata(DisplayUpdateReason.AUTOMATIC)
		);
		
		// Calculator model state.
		private bool initialState = true;								// Is calculator in the special "fresh" state?
        private List<string> inputList = new List<string>() { "0" };	// Current form of the user input (arithmetic expression).

		// Helper regexes.
		private Regex rgxContainsPoint = new Regex("\\.");				// Does the string contain a point?
        private Regex rgxEndsInPoint = new Regex("\\.$");				// Does the string end in a point?
		private Regex rgxIsSign = new Regex("^[+\\-*/]$");				// Is the string one of the arithmetic "signs" (operators)?
		private Regex rgxIsSignMultiplicative = new Regex("^[*/]$");	// Is the string one of the multiplicative arithmetic "signs" (operators)?

        public MainWindow() {
            InitializeComponent();

			// Set all program culture to be "invariant."
			// We do this in order for the program to not get confused by the decimal point/comma.
			Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
			Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;
        }
		
		// Reduces (calculates) the user input in the given range.
		// Returns the result.
		private string computeInputList(int startIndex, int endIndex) {
			for (int i = startIndex;i < endIndex;++i) {
				if (rgxIsSignMultiplicative.IsMatch(inputList[i])) {
					inputList[i - 1] = (inputList[i] == "*"
						? (decimal.Parse(inputList[i - 1]) * decimal.Parse(inputList[i + 1]))
						: (decimal.Parse(inputList[i - 1]) / decimal.Parse(inputList[i + 1]))).ToString();
						
					inputList.RemoveAt(i--);
					inputList.RemoveAt(i-- + 1);

					endIndex -= 2;
				}
			}
			
			for (int i = startIndex;i < endIndex;++i) {
				if (rgxIsSign.IsMatch(inputList[i])) {
					inputList[i - 1] = (inputList[i] == "+"
						? (decimal.Parse(inputList[i - 1]) + decimal.Parse(inputList[i + 1]))
						: (decimal.Parse(inputList[i - 1]) - decimal.Parse(inputList[i + 1]))).ToString();
						
					inputList.RemoveAt(i--);
					inputList.RemoveAt(i-- + 1);

					endIndex -= 2;
				}
			}
			
			return inputList[startIndex];
		}

		// Activated when the user clicks a number (digit).
		// The digit will either get appended to the current string of digits or will replace it.
        private void onButtonNumberClicked(object sender, RoutedEventArgs eventArgs) {
				var number = ((Button) sender).Content.ToString()!;
			
				if (initialState) {
					textBoxDisplay.SetValue(propertyDisplayUpdateReason, DisplayUpdateReason.MANUAL);
					textBoxDisplay.Text = number;
					inputList.Clear();
					inputList.Add(number);
				} else if (rgxIsSign.IsMatch(inputList[inputList.Count - 1])) {
					inputList.Add(number);
					textBoxDisplay.SetValue(propertyDisplayUpdateReason, DisplayUpdateReason.MANUAL);
					textBoxDisplay.Text = number;
				} else if (number != "0" && inputList[inputList.Count - 1] == "0") {
					inputList[inputList.Count - 1] = number;
					textBoxDisplay.SetValue(propertyDisplayUpdateReason, DisplayUpdateReason.MANUAL);
					textBoxDisplay.Text = number;
				} else if (inputList[inputList.Count - 1] != "0") {
					inputList[inputList.Count - 1] += number;
					textBoxDisplay.SetValue(propertyDisplayUpdateReason, DisplayUpdateReason.MANUAL);
					textBoxDisplay.Text += number;
				}
				
				initialState = initialState && number == "0";
        }
		
		// Activated when the user clicks one of the signs (operators).
		// The operator will get appended to the current user input, and previous user input may be reduced (calculated) at this stage.
		private void onButtonSignClicked(object sender, RoutedEventArgs eventArgs) {
			var sign = ((Button) sender).Content.ToString()!;
			
			if (rgxIsSign.IsMatch(inputList[inputList.Count - 1])) {
				inputList[inputList.Count - 1] = sign;
			} else if (rgxEndsInPoint.IsMatch(inputList[inputList.Count - 1])) {
				inputList.Add(sign);
				textBoxDisplay.SetValue(propertyDisplayUpdateReason, DisplayUpdateReason.MANUAL);
				textBoxDisplay.Text = textBoxDisplay.Text.Remove(textBoxDisplay.Text.Length - 1);
			} else {
				inputList.Add(sign);
			}
			
			if (inputList.Count > 3) {
				if (!rgxIsSignMultiplicative.IsMatch(sign)) {
					textBoxDisplay.SetValue(propertyDisplayUpdateReason, DisplayUpdateReason.AUTOMATIC);
					textBoxDisplay.Text = computeInputList(0, inputList.Count - 1);
				} else if (rgxIsSignMultiplicative.IsMatch(inputList[inputList.Count - 3])) {
					textBoxDisplay.SetValue(propertyDisplayUpdateReason, DisplayUpdateReason.AUTOMATIC);
					textBoxDisplay.Text = computeInputList(inputList.Count - 4, inputList.Count - 1);
				}
			}
			
			initialState = false;
		}
		
		// Activated when the user clicks one of the "special" buttons.
		// - . (Point): Append the decimal point to the current number in the user input, or append number "0." to the current user input.
		// - C (Clear): Reset the calculator. This will put the calculator in "fresh" state.
		// - = (Equals): Reduce (calculate) the whole expression. This will put the calculator in "fresh" state.
		private void onButtonSpecialClicked(object sender, RoutedEventArgs eventArgs) {
			if (sender == buttonPoint && !rgxContainsPoint.IsMatch(inputList[inputList.Count - 1])) {
				textBoxDisplay.SetValue(propertyDisplayUpdateReason, DisplayUpdateReason.MANUAL);
				
				if (rgxIsSign.IsMatch(inputList[inputList.Count - 1])) {
					inputList.Add("0.");
					textBoxDisplay.Text = "0.";
				} else {
					inputList[inputList.Count - 1] += ".";
					textBoxDisplay.Text += ".";
					initialState = false;
				}
			} else if (sender == buttonC) {
				textBoxDisplay.SetValue(propertyDisplayUpdateReason, DisplayUpdateReason.AUTOMATIC);
				textBoxDisplay.Text = "0";
				inputList.Clear();
				inputList.Add("0");
				initialState = true;
			} else if (sender == buttonEquals) {
				if (rgxIsSign.IsMatch(inputList[inputList.Count - 1])) {
					inputList.RemoveAt(inputList.Count - 1);
				}
				
				textBoxDisplay.SetValue(propertyDisplayUpdateReason, DisplayUpdateReason.AUTOMATIC);
				textBoxDisplay.Text = computeInputList(0, inputList.Count);
				inputList.Clear();
				inputList.Add(textBoxDisplay.Text);
				initialState = true;
			}
		}
		
		// Scrolls the display text box in the correct direction, based on the update reason.
		// The purpose of this scrolling is to make it more convenient for the user to look at the display.
		private void onTextBoxDisplayChanged(object sender, TextChangedEventArgs eventArgs) {
			if ((DisplayUpdateReason) textBoxDisplay.GetValue(propertyDisplayUpdateReason) == DisplayUpdateReason.MANUAL) {
				scrollViewerDisplay.ScrollToRightEnd();
			} else {
				scrollViewerDisplay.ScrollToLeftEnd();
			}
		}
    }
}