## Motivation

I created this project to fulfill a practical task of a job hiring process.

## Dependencies

- .NET 7.0

For more details you can check the project file of a project (`*.csproj`).

## Build & run

To build and run any project just enter its directory and run the following commands:

```
dotnet restore	# Generate all the dependency files.
dotnet build	# Build the application.
dotnet run		# Run the application.
```

## Notes

- All projects were tested only under Windows 10.
